//
//  CategoryCollectionViewCell.swift
//  Etrix_Demo
//
//  Created by Manish on 7/13/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell
{

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var baseImageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    var hexColorString : String = "EDEDED"

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        self.labelTitle.font = Constants.customFonts.sertigFontTitle
        self.labelTitle.textColor = Constants.customColors.grayColorBase
    }

    override func layoutSubviews()
    {
        
        super.layoutSubviews()
        self.baseView.backgroundColor = UIColor.init(hexString: "\(hexColorString)")
        self.baseView.makeMeRound(borderWidth: 0.0, borderColor: nil, backgroundColor: UIColor.init(hexString: "\(hexColorString)"))
    }
}
