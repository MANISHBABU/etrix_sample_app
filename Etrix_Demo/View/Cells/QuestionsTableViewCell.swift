//
//  QuestionsTableViewCell.swift
//  Etrix_Demo
//
//  Created by MANISH_iOS on 13/07/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import UIKit

class QuestionsTableViewCell: UITableViewCell
{

    @IBOutlet var arrowImageView: UIImageView!
    @IBOutlet var questionLabel: UILabel!
    @IBOutlet var answerLabel: UILabel!
    @IBOutlet var baseView: UIView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        self.questionLabel.font = Constants.customFonts.sertigFontMedium
        self.questionLabel.textColor = Constants.customColors.grayColorBase
        self.answerLabel.font = Constants.customFonts.sertigFont
        self.answerLabel.textColor = Constants.customColors.blackColorBase
    }
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.baseView.addShadowEffectToViewWithSize(1.0, size: 5.0, color: Constants.customColors.tabColorBase)
    }

}
