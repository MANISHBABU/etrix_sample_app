//
//  QuestionsViewController.swift
//  Etrix_Demo
//
//  Created by Manish on 7/13/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import UIKit

class QuestionsViewController: MasterViewController
{
    @IBOutlet var questionsTableView: UITableView!
    
    var questions: [QuestionsModel] = [QuestionsModel]()

    override func viewDidLoad()
    {
        mBackgroundColor = Constants.customColors.tabColorBase
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        preSetup()
    }
    
    // Setting up before view did appear
    private func preSetup()
    {
        preUI()
        //getUpdatedData()
    }
    // Pre user interface changes
    private func preUI()
    {
        self.title = "Questions"
        tableViewPreSetUp()
    }
    private func tableViewPreSetUp()
    {
        questionsTableView .register(UINib(nibName: "QuestionsTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.identifiersED.questionsTableViewCellID)
        questionsTableView.rowHeight = UITableViewAutomaticDimension
        questionsTableView.estimatedRowHeight = 180.0
        self.automaticallyAdjustsScrollViewInsets = false
    }
}
// MARK: - UITableViewDelegate, UITableViewDataSource
extension QuestionsViewController: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : QuestionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: Constants.identifiersED.questionsTableViewCellID, for: indexPath) as! QuestionsTableViewCell
        let question    = questions[indexPath.row]
        cell.questionLabel.text = question.question != nil ? question.question! : "N/A"
        
        if question.myAnswer != nil
        {
            cell.arrowImageView?.image = #imageLiteral(resourceName: "right_arrow_filled")
            cell.answerLabel.text = "My Answer -  \(question.myAnswer!)"
        }else
        {
            cell.arrowImageView?.image = #imageLiteral(resourceName: "right_arrow")
            cell.answerLabel.text = ""
        }
        cell.customResizeFix()
        
        return cell
    }
}
