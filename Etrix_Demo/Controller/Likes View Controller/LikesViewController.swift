//
//  LikesViewController.swift
//  Etrix_Demo
//
//  Created by Manish on 7/13/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import UIKit

class LikesViewController: MasterViewController {

    @IBOutlet var hLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = Constants.customColors.blackColorBase
        hLabel.makeMeCustom()

    }

}
