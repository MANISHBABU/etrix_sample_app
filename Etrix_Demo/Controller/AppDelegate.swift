//
//  AppDelegate.swift
//  Etrix_Demo
//
//  Created by Manish on 7/13/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate
{

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        // Override point for customization after application launch.
        // This will be responsible for initial setup before presenting initial view controller
        intialSetup()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
// MARK: - App Delegates Extension to initial and setup
extension AppDelegate
{
    //MARK:- Custom functions
    func intialSetup()
    {
        // Setting Up Navigation Bar Default 
        setUpNavBar()
        setUpTabBar()
        setUpInitialViewController()
    }

    func setUpNavBar()
    {
        UINavigationBar.appearance().barTintColor = Constants.customColors.blackColorBase
        UINavigationBar.appearance().isTranslucent = false
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
    // Here I am assigning custom fonts and attributes to tab bar item
    func setUpTabBar()
    {
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : Constants.customColors.redColorBase, NSFontAttributeName: Constants.customFonts.sertigFontTab!], for: .selected)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : Constants.customColors.grayColorBase, NSFontAttributeName: Constants.customFonts.sertigFontTab!], for: .normal)
        
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
    }

    // Call Landing Page if Auto Login else call Login screen
    func setUpInitialViewController()
    {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        // Profiles
        let profilesTab = Presenter.shared.getProfilePageVC()
        profilesTab.addImageAndTitleFotTab(title: "Profiles", imageIcon: #imageLiteral(resourceName: "profiles_inactive"), selImageIcon: #imageLiteral(resourceName: "profiles_active"), tagID: 0)
        
        // Questions
        let questionsTab = Presenter.shared.getLandinPageNVC()
        questionsTab.addImageAndTitleFotTab(title: "Questions", imageIcon: #imageLiteral(resourceName: "questions_inactive"), selImageIcon: #imageLiteral(resourceName: "questions_active"), tagID: 1)

        // Likes
        let likesTab = Presenter.shared.getLikesPageVC()
        likesTab.addImageAndTitleFotTab(title: "Likes", imageIcon: #imageLiteral(resourceName: "likes_inactive"), selImageIcon: #imageLiteral(resourceName: "likes_active"), tagID: 2)

        // Chat
        let chatTab = Presenter.shared.getChatPageVC()
        chatTab.addImageAndTitleFotTab(title: "Chat", imageIcon: #imageLiteral(resourceName: "chat_inactive"), selImageIcon: #imageLiteral(resourceName: "chat_active"), tagID: 3)

        // My Profile
        let myProfileTab = Presenter.shared.getMyProfilerPageVC()
        myProfileTab.addImageAndTitleFotTab(title: "My Profile", imageIcon: #imageLiteral(resourceName: "my_profile_inactive"), selImageIcon: #imageLiteral(resourceName: "my_profile_active"), tagID: 4)

        let tabBarControllerObj = UITabBarController() 
        tabBarControllerObj.viewControllers = [profilesTab, questionsTab, likesTab, chatTab, myProfileTab]
        tabBarControllerObj.selectedIndex = 1
        tabBarControllerObj.view.backgroundColor = Constants.customColors.tabColorBase
        window?.rootViewController = tabBarControllerObj
        self.window?.makeKeyAndVisible()
    }

}

