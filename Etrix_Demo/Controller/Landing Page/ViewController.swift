//
//  ViewController.swift
//  Etrix_Demo
//
//  Created by Manish on 7/13/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import UIKit

class ViewController: MasterViewController
{
    // IBOutlets
    @IBOutlet weak var collectionViewCategories: UICollectionView!
    @IBOutlet var searchBarCategory: UISearchBar!

    // Var
    let refreshControlColl = UIRefreshControl()
    var categoriesArrayFiltered: [CategoriesModel] = [CategoriesModel]()
    var isRemovingTextWithBackspace = false
    var shouldDismissKeyboard = false

    
    override func viewDidLoad()
    {
        shouldHideNavBarLeftBtn = true // This will hide the back button
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        preSetup()
    }

    // Setting up before view did appear
    private func preSetup()
    {
        preUI()
        getUpdatedData()
        searchBarCategory.delegate = self
    }
    // Pre user interface changes
    private func preUI()
    {
        self.title = "Categories"
        collViewPreSetUp()
        addRefreshControl()

    }
    private func collViewPreSetUp()
    {
        collectionViewCategories .register(UINib(nibName: "CategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: Constants.identifiersED.categoryCollectionCellID)
        (collectionViewCategories.collectionViewLayout as! UICollectionViewFlowLayout).minimumInteritemSpacing = 5.0
        (collectionViewCategories.collectionViewLayout as! UICollectionViewFlowLayout).minimumLineSpacing = 5.0
        (collectionViewCategories.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .vertical
        collectionViewCategories.allowsMultipleSelection = false
        collectionViewCategories.isPagingEnabled = false
        self.automaticallyAdjustsScrollViewInsets = false
    }
    /*!
     Adding refresh control for updating data on pull to refresh
     */
    private func addRefreshControl()
    {
        // Adding Refresh Control
        refreshControlColl.attributedTitle = refreshControlColl.pullToRefreshAttributedStringWithColor(color: Constants.customColors.blackColorBase, title: "Fetching Categories...")
        
        refreshControlColl.addTarget(self, action: #selector(ViewController.getUpdatedData), for: .valueChanged)
        
        refreshControlColl.tintColor = Constants.customColors.blackColorBase
        // Add to Table View
        if #available(iOS 10.0, *) {
            collectionViewCategories.refreshControl = refreshControlColl
        } else {
            collectionViewCategories.addSubview(refreshControlColl) // not required when using UITableViewController
        }
    }

    func getUpdatedData()
    {
        Webservices.shared.getRequest(Constants.serviceName.getCategories, parameters: nil, showLoader: true, passedView: self.view, passedText: "Getting Categories....", shouldAuthorize: false)
        { (responseObject, successStatus, errorMessage, timeline) in
            if self.refreshControlColl.isRefreshing
            {
                self.refreshControlColl.endRefreshing()
            }
            if responseObject != nil, successStatus == true
            {
                if Parser.parserCheckIfSuccess(responseObject: responseObject as! NSDictionary) == true, let categories = Parser.parserGetCategoryArray(responseObject: responseObject as! NSDictionary)
                {
                    Constants.categoriesArray = Parser.parseCategoriesResponse(categories: categories)
                    self.categoriesArrayFiltered = Constants.categoriesArray
                    self.collectionViewCategories.reloadData()
                }else
                {
                    self.showBasicAlert(title: "Failed", message: "Unable to get data, please try again later.")
                }
            }else
            {
                self.showBasicAlert(title: "Failed", message: errorMessage != nil ? errorMessage! : "Unable to get data, please try again later.")
            }
        }

    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    //MARK:- Collection view datasource
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = (Constants.screenSize.screenWidth - 20) / 4
        return CGSize(width: width, height: width * 1.35) // width & height are the same to make a square cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return  categoriesArrayFiltered.count
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.identifiersED.categoryCollectionCellID, for: indexPath) as! CategoryCollectionViewCell

        let category = categoriesArrayFiltered[indexPath.row]
        cell.baseImageView.setImageKingfisher(urlStr: category.iconURL)
        cell.labelTitle.text = category.nameCustom
        cell.hexColorString = category.colorHexString
        cell.customResizeFix()
        return cell
        
    }
    
    //MARK:- Collection view delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        searchBarCategory.resignFirstResponder()
        categoriesArrayFiltered = Constants.categoriesArray
        collectionViewCategories .reloadData()

        let modelCat = categoriesArrayFiltered[indexPath.row]
        if modelCat.questions.count > 0
        {
            Presenter.shared.pushQuestionsVC(target: self, questions: modelCat.questions)
        }else
        {
            self.showBasicAlert(title: "No Questions", message: "There are no questions available for this category")
        }
    }
}

extension ViewController: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        let text = (NSString(string: searchBar.text!)).replacingCharacters(in: range, with: text)
        
        self.isRemovingTextWithBackspace = (NSString(string: searchBar.text!).replacingCharacters(in: range, with: text).characters.count == 0)
      
        if text.characters.count > 10
        {
            return false
        }
        return true
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        shouldDismissKeyboard = false
        
        // If condition when user will click the clear button
        if searchText.characters.count == 0 && !isRemovingTextWithBackspace
        {
            shouldDismissKeyboard = true
        }else if searchText.characters.count >= 1
        {
            // Check for category search
            categoriesArrayFiltered .removeAll()
            let filteredCategory = Constants.categoriesArray.filter { $0.nameCustom .contains(searchText.uppercaseFirst) }.flatMap { $0 }
            categoriesArrayFiltered  = filteredCategory
            collectionViewCategories .reloadData()
        }else
        {
            categoriesArrayFiltered .removeAll()
            categoriesArrayFiltered = Constants.categoriesArray
            collectionViewCategories.reloadData()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        if shouldDismissKeyboard == true
        {
                shouldDismissKeyboard = false
                customDismiss()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        customDismiss()
    }
    
    func customDismiss()
    {
        self.view.endEditing(true)
        searchBarCategory.resignFirstResponder()
        categoriesArrayFiltered = Constants.categoriesArray
        collectionViewCategories.reloadData()
    }
}
