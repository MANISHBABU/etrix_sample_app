//
//  Models.swift
//  Etrix_Demo
//
//  Created by Manish on 7/13/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import Foundation

class CategoriesModel: NSObject
{
    
    override init()
    {
        super.init()
    }
    
    var nameCustom              : String!
    var iconURL                 : String!
    var colorHexString          : String!
    var questions               : [QuestionsModel]! // Array of questions for a category
}


class QuestionsModel: NSObject
{
    override init()
    {
        super.init()
    }
    
    var question                : String!
    var myAnswer                : String?
    
}
