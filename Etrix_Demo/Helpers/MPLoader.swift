//
//  MPLoader.swift
//  Etrix_Demo
//
//  Created by Manish on 7/13/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import UIKit


/// UIActivityIndicator, this class responsible for customized UIActivityIndicator
class MPLoader: NSObject {
    
    var windowView : UIView!
    var arrayOfViews : [UIView] = [UIView]()
    /// Share Instance to globaly use loader

    static let shared = MPLoader()

    /**
     Show the loader when any data coming from server taking time.
     
     - parameter text:    String to show while loader rotate
     - parameter blockUI: Boolean value to hide or show loader
     */
    func showLoader(_ text : String, blockUI : Bool, passedView: UIView?, withCustomImage: Bool)
    {
        if passedView == nil
        {
            windowView = UIView(frame: UIScreen.main.bounds)
        }else
        {
            windowView = UIView(frame: (passedView?.bounds)!)
        }
        
        let container: UIView = UIView()
        container.frame = windowView.frame
        container.center = windowView.center
        container.backgroundColor = UIColor.black .withAlphaComponent(0.7)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = windowView.center
        //        loadingView.backgroundColor = Macros.Colors.yellowColor .withAlphaComponent(0.85)
        loadingView.backgroundColor = UIColor.black .withAlphaComponent(0.80)
        
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        let customIndicatorImgView: UIImageView = UIImageView()
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()

        actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            actInd.activityIndicatorViewStyle =
                UIActivityIndicatorViewStyle.whiteLarge
            actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
            actInd.color = Constants.customColors.blackColorBase
            
            loadingView.addSubview(actInd)
            container.addSubview(loadingView)

        
        windowView.addSubview(container)
        windowView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        if passedView == nil
        {
            self.window() .addSubview(windowView)
        }else
        {
            passedView?.addSubview(windowView)
        }
        if withCustomImage == true
        {
            customIndicatorImgView.startAnimating()
        }else
        {
            actInd.startAnimating()
        }

        arrayOfViews .append(windowView)
    }
    
    /**
     Hide the loader when any data recived completely.
     */
    func hideLoader()
    {
        //        OperationQueue.main.addOperation({() -> Void in
        
        if windowView != nil
        {
            arrayOfViews .removeObject(windowView)
            windowView .removeFromSuperview()
        }
        
        for loaderView in arrayOfViews
        {
            loaderView .removeFromSuperview()
        }
        // })
    }
    
    func window() -> UIView
    {
        return (UIApplication.shared.delegate as! AppDelegate).window!
    }
    
    func getUIColorFromHexForLoader(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    func animateArrayOfImages(forName : String, count : Int, imageV : UIImageView, shouldStartAnimation : Bool)
    {
        //add images to the array
        var imagesListArray  = [UIImage]()
        //use for loop
        for position in 1...count
        {
            
            let strImageName : String = "\(forName)\(position)"
            let image  = UIImage(named:strImageName)
            imagesListArray.append(image!)
        }
        
        imageV.animationImages = imagesListArray;
        imageV.animationDuration = 1.0
        if shouldStartAnimation == true
        {
            imageV.startAnimating()
        }
    }

}
