//
//  Extensions.swift
//  Etrix_Demo
//
//  Created by Manish on 7/13/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher


// MARK: - Extensions 1. UIViewController
extension UIViewController
{
    
    /// This function is used to perform delay action
    ///
    /// - Parameters:
    ///   - delay: decimal value (0.75 || 3.0)
    ///   - closure: NI
    func delayCustom(_ delay:Double, closure:@escaping ()->())
    {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    
    /// This function is used to show alet controller to the view controller which is on top of the view controller hierarchy.
    ///
    /// - Parameters:
    ///   - title: Title of the alert view controller
    ///   - message: Message of the alert view controller
    func showBasicAlert(title : String?, message : String)
    {
        // Show alert
        let alertController: UIAlertController = UIAlertController(title: title == nil ? Constants.mainAppName : title!, message: message, preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "OK", style: .default, handler: { action in
            alertController .dismiss(animated: true, completion: nil)
        })
        alertController.addAction(actionOk)
        self.present(alertController, animated: true, completion: nil)
    }
    
    /// Basic navigation bar throughout the project
    func myBasicNavBar()
    {
        if self.navigationController != nil, self.navigationController?.navigationBar != nil
        {
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : Constants.customColors.whiteColorBase, NSFontAttributeName: Constants.customFonts.sertigFontTitle!]
        }
    }
    
    
    /// Setup Tab bar title, image, selected image
    ///
    /// - Parameters:
    ///   - title: Title on tab
    ///   - imageIcon: Default Image Icon
    ///   - selImageIcon: Selected Image Icon
    func addImageAndTitleFotTab(title: String, imageIcon: UIImage?, selImageIcon: UIImage?, tagID : Int)
    {
        self.tabBarItem = UITabBarItem(title: title, image: imageIcon?.withRenderingMode(.alwaysOriginal), selectedImage: selImageIcon?.withRenderingMode(.alwaysOriginal))

        self.tabBarItem.tag = tagID
    }
}

// MARK: - UIView
extension UIView
{
    
    /// This function will make a UIView round (Circle)
    ///
    /// - Parameters:
    ///   - borderWidth: Width which you want at circumference of the circle
    ///   - borderColor: The color which you want to give to border
    public func makeMeRound(borderWidth : CGFloat, borderColor : UIColor!, backgroundColor : UIColor)
    {
        self.layer.cornerRadius = self.frame.size.width/2
        
        self.clipsToBounds = true
        
        if borderColor != nil
        {
            self.layer.borderColor = borderColor.cgColor
        }
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
        self.backgroundColor = backgroundColor
        self.contentMode = .scaleAspectFit
        
    }
    
    
    /// This function will apply shadow to a view (But it is not recommended, use Bezier Path)
    ///
    /// - Parameters:
    ///   - cornerRadius: corner radius of view
    ///   - size: offset size
    ///   - color: shadow and border color
    func addShadowEffectToViewWithSize(_ cornerRadius : CGFloat, size : CGFloat, color: UIColor, customBorder: CGFloat = 1)
    {
        if cornerRadius > 0.0
        {
            // corner radius
            self.layer.cornerRadius = cornerRadius
            self.layer.borderColor = color.cgColor
            //border
            self.layer.borderWidth = customBorder
        }
        
        //shodow effect
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        self.layer.shadowOpacity = 0.50
        self.layer.shadowRadius = size
    }

}
extension UITableViewCell
{
    // Fixing autolayout for custom cells
    func customResizeFix()
    {
        self.backgroundColor = self.contentView.backgroundColor
        self.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        self.contentView.clipsToBounds = true
        self.layoutIfNeeded()
    }
}
extension UICollectionViewCell
{
    // Fixing autolayout for custom cells
    func customResizeFix()
    {
        self.backgroundColor = self.contentView.backgroundColor
        self.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        self.contentView.clipsToBounds = true
        self.layoutIfNeeded()
    }
}


// MARK: - UIImageView
extension UIImageView
{
    
    /// This function will download image from server using Kingfisher library
    ///
    /// - Parameter urlStr: URL of the image
    func setImageKingfisher(urlStr : String)
    {
        self.kf.indicatorType = .activity
        let urlConverted = URL(string: "\(urlStr)")!
        self.kf.setImage(with: urlConverted, placeholder: #imageLiteral(resourceName: "profiles_inactive"))
    }
}
//MARK:- UILabel
extension UILabel
{
    func makeMeCustom()
    {
        self.textColor = Constants.customColors.whiteColorBase
        self.addShadowEffectToViewWithSize(self.frame.size.height / 2, size: 10.0, color: Constants.customColors.redColorBase, customBorder: 3.0)
    }
}

//MARK:- String
extension String
{
    var first: String
    {
        return String(characters.prefix(1))
    }
    var uppercaseFirst: String
    {
        return first.uppercased() + String(characters.dropFirst())
    }
}

// MARK: - UIColor
extension UIColor
{
    
    /// Change hex code to color while initialization
    ///
    /// - Parameter hexString: hexString of the color
    convenience init(hexString: String)
    {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

// MARK: - Array Extension
extension Array where Element: Equatable {
    
    /// This extension will remove passed element from the array list
    ///
    /// - Parameter object: Object from array which needs to be removed.
    mutating func removeObject(_ object: Element) {
        if let index = self.index(of: object) {
            self.remove(at: index)
        }
    }
    
    /// This function will remove set of objects from Array
    ///
    /// - Parameter array: set of objects from the Array List.
    mutating func removeObjectsInArray(_ array: [Element]) {
        for object in array {
            self.removeObject(object)
        }
    }
}


// MARK: - UIRefreshControl Extension
extension UIRefreshControl
{
    
    /// Pull to refresh text and its color
    ///
    /// - Parameters:
    ///   - color: Color for Attributed text
    ///   - title: Attributed text which will be set as a title
    /// - Returns: NSAttributedString with updated color and title text for pull to refresh
    func pullToRefreshAttributedStringWithColor(color:UIColor, title: String) -> NSAttributedString {
        return NSAttributedString(string: title, attributes: [
            NSForegroundColorAttributeName:color
            ])
    }
    
}
