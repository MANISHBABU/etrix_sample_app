//
//  Presenter.swift
//  Etrix_Demo
//
//  Created by Manish on 7/13/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import Foundation
import UIKit

class Presenter
{
    static let shared = Presenter()
    let mainStoryBoard          = UIStoryboard(name: "Main", bundle: nil)
    let otherTabsStoryBoard     = UIStoryboard(name: "OtherTabs", bundle: nil)

    var landingPageVCObj    : ViewController!
    var landingPageNVCObj   : UINavigationController!
    var profilesVCObj       : ProfilesViewController!
    var likesVCObj          : LikesViewController!
    var chatVCObj           : ChatsViewController!
    var myProfileVCObj      : MyProfileViewController!

    //MARK:- Get View Controllers
    
    /// To get Landing Page View Controller
    ///
    /// - Returns: ViewController
    func getLandinPageVC() -> ViewController
    {
        if landingPageVCObj == nil
        {
            landingPageVCObj = mainStoryBoard.instantiateViewController(withIdentifier: Constants.identifiersED.landingPageVCID) as! ViewController
        }
        return landingPageVCObj
    }
    func getLandinPageNVC() -> UINavigationController
    {
        if landingPageNVCObj == nil
        {
            landingPageVCObj = mainStoryBoard.instantiateViewController(withIdentifier: Constants.identifiersED.landingPageVCID) as! ViewController
        }
        if landingPageNVCObj == nil
        {
            landingPageNVCObj = UINavigationController(rootViewController: landingPageVCObj)
        }

        return landingPageNVCObj
    }

    /// To get Profiles View Controller
    ///
    /// - Returns: ProfilesViewController
    func getProfilePageVC() -> ProfilesViewController
    {
        if profilesVCObj == nil
        {
            profilesVCObj = otherTabsStoryBoard.instantiateViewController(withIdentifier: Constants.identifiersED.profilesVCID) as! ProfilesViewController
        }
        return profilesVCObj
    }
    
    /// To get Likes View Controller
    ///
    /// - Returns: LikesViewController
    func getLikesPageVC() -> LikesViewController
    {
        if likesVCObj == nil
        {
            likesVCObj = otherTabsStoryBoard.instantiateViewController(withIdentifier: Constants.identifiersED.likesVCID) as! LikesViewController
        }
        return likesVCObj
    }
    
    /// To get Chats View Controller
    ///
    /// - Returns: ChatsViewController
    func getChatPageVC() -> ChatsViewController
    {
        if chatVCObj == nil
        {
            chatVCObj = otherTabsStoryBoard.instantiateViewController(withIdentifier: Constants.identifiersED.chatVCID) as! ChatsViewController
        }
        return chatVCObj
    }
    
    /// To get My Profile View Controller
    ///
    /// - Returns: MyProfileViewController
    func getMyProfilerPageVC() -> MyProfileViewController
    {
        if myProfileVCObj == nil
        {
            myProfileVCObj = otherTabsStoryBoard.instantiateViewController(withIdentifier: Constants.identifiersED.myProfileVCID) as! MyProfileViewController
        }
        return myProfileVCObj
    }


    
    // Navigation
    func pushQuestionsVC(target : UIViewController, questions: [QuestionsModel])
    {
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: Constants.identifiersED.QuestionsVCID) as! QuestionsViewController
        vc.questions =  questions
        target.navigationController?.pushViewController(vc, animated: true)
    }
}
