//
//  Parser.swift
//  Etrix_Demo
//
//  Created by Manish on 7/13/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import Foundation

class Parser: NSObject
{
    
    /// It will check if our request is successful or not
    ///
    /// - Parameter responseObject: response object of api hit
    /// - Returns: true if success, false if failed
    class func parserCheckIfSuccess(responseObject :  NSDictionary) -> Bool
    {
        if let getStatus =  responseObject.value(forKey: "status") as? String, getStatus == "success"
        {
            return true
        }else
        {
            return false
        }
    }
    
    /// It will check if our request is successful or not
    ///
    /// - Parameter responseObject: response object of api hit
    /// - Returns: true if success, false if failed
    class func parserGetCategoryArray(responseObject :  NSDictionary) -> [NSDictionary]!
    {

        if let categories  = responseObject.value(forKey: "categories") as? [NSDictionary]!
        {
            return categories
            
        }else
        {
            return nil
        }
    }

    class func parseCategoriesResponse(categories :  [NSDictionary]) -> [CategoriesModel]
    {
        var catModels = [CategoriesModel]()
        
        for category in categories
        {
            let categoryData = category as AnyObject
            let catModel = CategoriesModel()
            
            if let name =  categoryData.value(forKey: "name") as? String
            {
                catModel.nameCustom = name
            }
            if let iconURL =  categoryData.value(forKey: "icon") as? String
            {
                catModel.iconURL = "\(iconURL)"
            }
            if let colorHexString =  categoryData.value(forKey: "color") as? String
            {
                catModel.colorHexString = "\(colorHexString)"
            }
            
            // Question Parsing
            if let questions =  categoryData.value(forKey: "questions") as? [NSDictionary]
            {
                catModel.questions = [QuestionsModel]()
                for question in questions
                {
                    let questionModelObj = QuestionsModel()
                    
                    if let questionDescp =  question.value(forKey: "question") as? String
                    {
                        questionModelObj.question = questionDescp
                    }
                    if let myAnswer =  question.value(forKey: "myAnswer") as? String
                    {
                        questionModelObj.myAnswer = myAnswer
                    }else
                    {
                        questionModelObj.myAnswer = nil
                    }
                    
                    catModel.questions.append(questionModelObj)
                }
            }


            catModels.append(catModel)
        }

        return catModels
    }
}
